<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		
		//authorizatin();
		
		//echo "In Admin Controller";
		
	}
	
	public function index() {
		
		//echo $this->session->userdata("username");
		
		//$this->load->view('myviews/dashboard');
		
		$this->load->template('myviews/dashboard', ["title" => "Dashboard"]);
		
	}
	
	public function logout() {
		
		$this->session->unset_userdata('username');
		
		redirect("/login");
	}
	
	public function adduser() {
		
		$this->load->template('myviews/adduser', ["title" => "Add New User"]);
		
	}
	
	public function form() {
		$this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');
		
		$this->load->view('form');
		
	}
	
	public function form_action() {
		
		$this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');
		
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('mobileno', 'Phone Number', 'trim|required|numeric|min_length[10]|max_length[12]');
		$this->form_validation->set_rules('password', 'Password', 'trim|alpha_numeric|required|min_length[8]');
		
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('form');
		} else {
			
			$form_params = $this->input->post();
		
			echo "<pre>";
			var_dump($form_params);
			echo "</pre>";
			
			echo "Form submitted successfully...";
			
		}
		
		
	}
	
}