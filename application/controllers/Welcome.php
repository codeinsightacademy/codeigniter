<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function index()
	{
		$this->load->view('welcome_message');
	}
	
	public function test() {
		
		$this->load->model('TestModel');
		
		$msg = $this->TestModel->testModelFun();
		
		//echo $msg; exit;
		
		//echo "Hello CI";
		
		$data = [];
		$data["msg"] = $msg;
		$data["msg2"] = "Hello View I am from controller<br>";
		
		$this->load->view("myviews/test", $data);
	}
	
	public function sqr_form() {
		
		$this->load->view("myviews/sqr_form");
		
	}
	
	public function sqr() {
		$num = $this->input->post('num', TRUE);
		//$num = $this->input->get('num', TRUE);
		//$this->input->get(); // to get all paramters in array
		
		$sqr = $num * $num;
		
		echo "Square of $num is $sqr";
		
	}
	
	public function dbtest() {
		
		$result = $this->db->get("users");
		
		foreach ($result->result() as $user)
		{
			var_dump($user->name); // access attributes
		}
		
	}
	
	public function dbselect() {
		
		$result2 = $this->db->query("SELECT * FROM users WHERE city LIKE '%Delhi%'");
		
		echo "<pre>";
		
		print_r($result2->result('array'));
		
	}
	
	public function dbdelete() {
		$id = $this->input->get('id');
		
		$result = $this->db->query("DELETE FROM users WHERE id = '$id'");
		
		var_dump($result);
		
	}
	
	public function dbinsert() {
		
		$username = $this->input->get('username');
		
		$sql = "INSERT INTO `users` (`id`, `username`, `password`, `name`, `age`, `city`, `added_date`, `updated_date`, `status`) VALUES (NULL, '$username', 'asdfasdf', 'ciname', '25', 'Mumbai', NOW(), NOW(), 'active')";
		
		$result = $this->db->query($sql);
		
		var_dump($result);
		
	}
	
	public function dbupdate() {
		
		$sql = "UPDATE `users` SET `age` = '33' WHERE `users`.`id` = 24";
		
		$result = $this->db->query($sql);
		
		var_dump($result);
		
	}
	
	public function testhelper() {
		
		//$this->load->helper('url');
		$this->load->helper('html');
		$this->load->helper('download');
		
		echo site_url();
		
		echo br(10);
		//echo "<br>";
		
		echo base_url();
		
		$data = 'Here is some text!';
		$name = 'mytext.txt';
		//force_download($name, $data);
		
		echo br();
		
		//$this->load->helper('mymath');
		
		echo add(5, 6) . br();
		echo mul(5, 6);
		
	}
	
	public function testlib() {
		$this->load->library('pagination');

		$config['base_url'] = base_url();
		$config['total_rows'] = 200;
		$config['per_page'] = 20;

		$this->pagination->initialize($config);

		echo $this->pagination->create_links();
		
		
		//$this->load->library('circle');
		
		$this->circle->area();
		$this->circle->circumference();
		
	}
	
	public function testconfig() {
		
		//$this->config->load('messages');
		
		$msg = $this->config->item('msg');
		
		var_dump($this->config->item('allow_get_array'));
		
		echo $msg;
		
		
	}
	
	public function testsession() {
		//$this->load->library('session');
		$this->session->set_userdata(["username" => "AdminUser"]);
		
	}
}
