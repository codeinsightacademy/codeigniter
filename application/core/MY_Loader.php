<?php

class MY_Loader extends CI_Loader {
    public function template($template_name, $vars = array(), $return = FALSE) {
        $this->view('myviews/header', $vars);
        $this->view($template_name, $vars);
        $this->view('myviews/footer', $vars);
    }
}